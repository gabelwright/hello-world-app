package com.example.helloworld;

import android.annotation.SuppressLint;
import android.util.Log;

class Timer {
    private long startTime;

    Timer(){
        this.startTime = 0;
    }

    void start(){
        this.startTime = System.currentTimeMillis();
        Log.d("mgw", this.startTime + "");
    }

    void stop(){
        this.startTime = 0;
    }

//    public long elapsedTime(){
//        return System.currentTimeMillis() - this.startTime;
//    }

    @SuppressLint("DefaultLocale")
    String elapsedTime(){
        long stopTime = System.currentTimeMillis();
        long milli = stopTime - this.startTime;
        Log.d("mgw", this.startTime + "");
        Log.d("mgw", stopTime + "");
        Log.d("mgw", milli + "");
        long sec = milli / 1000;
        long min = sec / 60;
        long hr = min / 60;
        sec = sec % 60;
        min = min % 60;
        return String.format("%01d", hr) + ":" + String.format("%02d", min) + ":" + String.format("%02d", sec);
    }

    boolean isRunning(){
        return this.startTime > 0;
    }
}