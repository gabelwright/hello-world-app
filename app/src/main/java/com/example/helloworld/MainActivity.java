package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Timer timer = new Timer();
    final Handler handler = new Handler();
    final int delay = 1000; //milliseconds
    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.start).setEnabled(true);
        findViewById(R.id.stop).setEnabled(false);

    }

    public void startClick(View view){
        timer.start();
        findViewById(R.id.start).setEnabled(false);
        findViewById(R.id.stop).setEnabled(true);
        handler.postDelayed(new Runnable(){
            public void run(){
                updateDisplay();
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    public void stopClick(View view){
        timer.stop();
        findViewById(R.id.start).setEnabled(true);
        findViewById(R.id.stop).setEnabled(false);
    }

    public void updateDisplay(){
        if (timer.isRunning())
            ((TextView)findViewById(R.id.time)).setText(timer.elapsedTime());
    }






}
